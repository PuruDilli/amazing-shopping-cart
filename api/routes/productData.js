const DemoData = require('../demo');
const demoData = require('../data/students.json');

class ProductData extends DemoData {
  constructor() {
    super(demoData);
  }
}

module.exports = ProductData;
