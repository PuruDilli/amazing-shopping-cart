'use strict';

angular.module('amazingShopingCart')
.factory('myService', ['$resource',function($resource) {
	 var data = $resource('searchProduct', {},{query: {method:'get', isArray: true}});
      return data;
}]);
