'use strict';

angular.module('amazingShopingCart')
.factory('myFactory', ['$http',function($http) {
	function getData(){
      var url = 'product_items';
      return $http.get(url);
    }
  return{
  	getApiData:getData
  }
}]);
.factory('cFactory', ['$http', function($http) {
	return $http.get('http://localhost:9000/data_items');
}]);
