var express = require('express');
var app     = express();
const router = express.Router();
const ProductData = require('./productData');
const productData = new ProductData();

router.get('/',function(req,res){
	res.render('index');
});

router.get('/product_items', (req, res) => {
	res.send(productData);
});

router.get('/searchProduct', (req, res) => {
	res.send(productData.find(req.query));
});

router.get('/product_items/:state', (req, res) => {
	res.send(productData.findOne({ state: req.params.state }));
});

module.exports = router;