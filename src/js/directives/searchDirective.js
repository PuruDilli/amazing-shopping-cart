angular.module('amazingShopingCart')
.directive("search",function(){
	return{
		restrict:"EC",
		templateUrl:'./templates/searchTemplate.html',
		controller:'searchController'
	}
});
