var express = require('express');
var home = require('./routes/home');
var app     = express();

app.use(express.static('.build'));

app.use('/', home);

app.listen(9000, function() {
    console.log("Listening on 9000");
});
