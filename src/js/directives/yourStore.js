angular.module('amazingShopingCart')
    .directive("yourStore", function(){
    	return 
    	{
    		restrict: "EC";
    		template:'<div><a href="#/yourStore">yourStore</div>'
    	}
    });